package pl.beks.rental.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.beks.rental.DAO.ICityDAO;
import pl.beks.rental.DAO.IScooterDAO;
import pl.beks.rental.entities.City;
import pl.beks.rental.entities.Scooter;
import pl.beks.rental.model.ScooterFormModel;

import java.util.List;

@Controller
@RequestMapping("/admin/city")
public class AdminControllerCityScooter {

    @Autowired
    private IScooterDAO scooterDAO;

    @Autowired
    private ICityDAO cityDAO;

    @ModelAttribute("listCities")
    public Iterable<City> listCities() {
        return cityDAO.findAll();
    }


    @GetMapping(path = "/{cityId}")
    public String listAllScooters(@PathVariable("cityId") Long cityId, Model model) {
        List<Scooter> all = scooterDAO.findAllByCity_Id(cityId);
        City city = cityDAO.findOne(cityId);
        model.addAttribute("scooters", all);
        model.addAttribute("cityName", city.getName());
        model.addAttribute("cityId", cityId);
        model.addAttribute("city", city);
        return "listCityScooters";
    }
    @GetMapping(path = "/{cityId}/addScooter")
    public String addScooter(@PathVariable("cityId") Long cityId){

        Scooter entity = Scooter.builder()
                .city(cityDAO.findOne(cityId))
                .build();
        scooterDAO.save(entity);
        return "redirect:/admin/city/"+cityId;
    }
    @GetMapping(path = "/{cityId}/deleteScooter/{scooterId}")
        public String deleteScooter(@PathVariable("scooterId") Long scooterId, @ModelAttribute("cityId") Long cityId){
        if (scooterDAO.findOne(scooterId)==null){
            return "notExistCity";
        }
        scooterDAO.delete(scooterId);
        return "redirect:/admin/city/"+cityId;
}
    @GetMapping(path = "/{cityId}/editScooter/{scooterId}")
    public String editScooter(@PathVariable("cityId") Long cityId, @PathVariable("scooterId") Long scooterId, Model model){
        Scooter scooter = scooterDAO.findOne(scooterId);
        if (scooter== null){
            return "notExist";
        }
        ScooterFormModel scooterFormModel = new ScooterFormModel(scooter);
        model.addAttribute("editScooter", scooterFormModel);
        model.addAttribute("fromCityId", cityId);

        return "editScooter";

    }
    @PostMapping(path = "/{cityId}/saveOldScooter/{scooterId}")
    public String saveEditScooter(@PathVariable("cityId") Long cityId, @PathVariable("scooterId") Long scooterId, ScooterFormModel editScooter){
        Scooter entity = scooterDAO.findOne(scooterId);
        if (entity==null){
            return "notExist";
        }
        entity.setCity(cityDAO.findOne(editScooter.getCityId()));
        scooterDAO.save(entity);
        return "redirect:/admin/city/"+cityId;
    }
}