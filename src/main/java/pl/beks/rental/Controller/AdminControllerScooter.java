package pl.beks.rental.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.beks.rental.DAO.ICityDAO;
import pl.beks.rental.DAO.IScooterDAO;
import pl.beks.rental.entities.City;
import pl.beks.rental.entities.Scooter;
import pl.beks.rental.model.ScooterFormModel;

@Controller
@RequestMapping("admin/scooter")
public class AdminControllerScooter {

    @Autowired
    private IScooterDAO scooterDAO;

    @Autowired
    private ICityDAO cityDAO;

    @ModelAttribute("listCities")
    public Iterable<City> listCities() {
        return cityDAO.findAll();
    }

    @GetMapping
    public String listAllScooters(Model model){
        Iterable<Scooter> all = scooterDAO.findAll();
        model.addAttribute("scooters", all);
        return "listScooters";
    }
    @GetMapping(path = "/addScooter")
    public String addScooter(Model model){
        ScooterFormModel newScooter = new ScooterFormModel();
        model.addAttribute("newScooter", newScooter);
        return "newScooter";
    }
    @PostMapping(path = "/saveNewScooter")
    public String saveScooter(@ModelAttribute("newScooter") ScooterFormModel newScooter){
        Scooter entity = Scooter.builder()
                .city(cityDAO.findOne(newScooter.getCityId()))
                .build();
        scooterDAO.save(entity);
        return "redirect:/admin/scooter";
    }
    @GetMapping(path = "/deleteScooter/{scooterId}")
    public String deleteScooter(@PathVariable("scooterId") Long scooterId){
        if (scooterDAO.findOne(scooterId)==null){
            return "notExistScooter";
        }
        scooterDAO.delete(scooterId);
        return "redirect:/admin/scooter";
    }
    @GetMapping(path = "editScooter/{scooterId}")
    public String editScooter(@PathVariable("scooterId") Long scooterId, Model model){
        Scooter scooter = scooterDAO.findOne(scooterId);
        if (scooter== null){
            return "notExistScooter";
        }
        ScooterFormModel scooterFormModel = new ScooterFormModel(scooter);
        model.addAttribute("editScooter", scooterFormModel);

        return "editScooter";

    }
    @PostMapping(path = "saveOldScooter/{scooterId}")
    public String saveEditScooter(@PathVariable("scooterId") Long scooterId, ScooterFormModel editScooter){
        Scooter entity = scooterDAO.findOne(scooterId);
        if (entity==null){
            return "notExistScooter";
        }
        entity.setCity(cityDAO.findOne(editScooter.getCityId()));
        scooterDAO.save(entity);
        return "redirect:/admin/scooter";
    }
}
