package pl.beks.rental.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.beks.rental.DAO.IUsersDAO;

@Controller
@RequestMapping("/admin/users")
public class AdminControllerUsers {

    @Autowired
    private IUsersDAO usersDAO;

    @GetMapping
    public String listUsers(Model model){
        model.addAttribute("listUsers", usersDAO.findAll());
        return "listUsers";
    }
}
