package pl.beks.rental.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.beks.rental.DAO.IUsersDAO;
import pl.beks.rental.entities.Users;
import pl.beks.rental.model.UsersFormModel;

@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private IUsersDAO usersDAO;

    @GetMapping
    public String addUser(Model model){
        UsersFormModel newUser = new UsersFormModel();
        model.addAttribute("newUser", newUser);
        return "Registration";

    }
    @PostMapping(path = "/saveUser")
    public String saveUser(@ModelAttribute("newUser") UsersFormModel newUser, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "Registration";
        }
        Users entity = Users.builder()
                .login(newUser.getLogin())
                .password(newUser.getPassword())
                .build();
        usersDAO.save(entity);
        return "redirect:/user";
    }

}
