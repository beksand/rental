package pl.beks.rental.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import pl.beks.rental.DAO.ICityDAO;
import pl.beks.rental.DAO.IScooterDAO;
import pl.beks.rental.entities.City;
import pl.beks.rental.model.CityFormModel;



@Controller
@RequestMapping("/admin")
public class AdminControllerCity {

    @Autowired
    private ICityDAO cityDAO;

    @Autowired
    private IScooterDAO skuterDAO;


    @GetMapping
    public String listCities(Model model){
        Iterable<City> all = cityDAO.findAll();
        model.addAttribute("cities", all);
        return "listCity";
    }
    @GetMapping(path = "/addCity")
    public String addCity(Model model){
        CityFormModel newCity = new CityFormModel();
        model.addAttribute("newCity", newCity);
        return "newCity";
    }
    @PostMapping(path = "/saveNewCity")
    public String saveCity(@Validated @ModelAttribute("newCity") CityFormModel newCity, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "newCity";
        }
        City entity = City.builder()
                .name(newCity.getName())
                .build();
        cityDAO.save(entity);
        return "redirect:/admin";
    }


    @GetMapping(path = "/deleteCity/{cityId}")
    public String deleteCity(@PathVariable("cityId") Long cityId){
        if (cityDAO.findOne(cityId)==null){
            return "notExistCity";
        }
        cityDAO.delete(cityId);
        return "redirect:/admin";
    }
    @GetMapping(path = "editCity/{cityId}")
    public String editCity(@PathVariable("cityId") Long cityId, Model model){
        City city = cityDAO.findOne(cityId);
        if (city== null){
            return "notExistCity";
        }
        model.addAttribute("editCity", city);

        return "editCity";

    }
    @PostMapping(path = "saveOldCity/{cityId}")
    public String saveEditCity(@PathVariable("cityId") Long cityId, City editCity){
        City cityBase = cityDAO.findOne(cityId);
        if (cityBase==null){
            return "notExistCity";
        }
        cityBase.setName(editCity.getName());
        cityDAO.save(cityBase);
        return "redirect:/admin";
    }
}
