package pl.beks.rental.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.beks.rental.DAO.ICityDAO;
import pl.beks.rental.DAO.IRentsDAO;
import pl.beks.rental.DAO.IScooterDAO;
import pl.beks.rental.entities.City;
import pl.beks.rental.entities.Rents;
import pl.beks.rental.entities.Scooter;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private ICityDAO cityDAO;

    @Autowired
    private IScooterDAO scooterDAO;

    @Autowired
    private IRentsDAO rentsDAO;


    @GetMapping
    public String listCitiesUser(Model model){
        Iterable<City> all = cityDAO.findAll();
        model.addAttribute("cities", all);
        return "listCityUser";
    }
    @GetMapping(path = "/city/{cityId}")
    public String questionOfRent(@PathVariable("cityId") Long cityId, Model model){
        model.addAttribute("cityId", cityId);
        List<Scooter> allByCityId = scooterDAO.findAllByCity_Id(cityId);
        model.addAttribute("listScooter", allByCityId);
        return "questionOfRent";
    }


    @GetMapping(path = "/city/{cityId}/toRent")
    public String toRent(@PathVariable("cityId") Long cityId, Model model){
        List<Scooter> scooterList = scooterDAO.findAllByCity_Id(cityId);
        model.addAttribute("cityId", cityId);
        for (Scooter scooter: scooterList
             ) {
            if (!scooter.isRented()){
                scooter.setRented(true);
                scooter.setDateTimeRented(LocalDateTime.now());
                scooterDAO.save(scooter);

                model.addAttribute("scooterId", scooter.getId());
                return "toRent";
            }
        }

        return "notExistToRent";
    }

    @GetMapping(path = "/city/{cityId}/toRent/{scooterId}/return")
    public String saveRented(@PathVariable("scooterId") Long scooterId, Model model){
        Scooter rentScooter = scooterDAO.findOne(scooterId);
        LocalDateTime startRent = rentScooter.getDateTimeRented();
        LocalDateTime finishRent = LocalDateTime.now();


        Rents rents = new Rents();
        rents.setScooter(rentScooter);
        rents.setStartRent(startRent);
        rents.setFinishRent(finishRent);
        rents.setProfit(50.0);
        rentsDAO.save(rents);


        Scooter scooter = scooterDAO.findOne(scooterId);
        scooter.setDateTimeRented(null);
        scooter.setRented(false);
        scooterDAO.save(scooter);
        model.addAttribute("money", rents.getProfit());
        return "payMoney";
    }
}
