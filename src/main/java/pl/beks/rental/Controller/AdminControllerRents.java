package pl.beks.rental.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.beks.rental.DAO.ICityDAO;
import pl.beks.rental.DAO.IRentsDAO;
import pl.beks.rental.DAO.IScooterDAO;

@Controller
@RequestMapping("/admin/rents")
public class AdminControllerRents {

    @Autowired
    private ICityDAO cityDAO;

    @Autowired
    private IScooterDAO scooterDAO;

    @Autowired
    private IRentsDAO rentsDAO;

    @GetMapping
    public String listRents(Model model){
        model.addAttribute("rents", rentsDAO.findAll());
        return "listRents";
    }
    @GetMapping(path = "/deleteRent/{rentId}")
    public String deleteRent(@PathVariable("rentId") Long rentId){
        rentsDAO.delete(rentId);
        return "redirect:/admin/rents";
    }

}
