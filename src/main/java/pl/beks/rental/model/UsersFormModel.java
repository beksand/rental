package pl.beks.rental.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import pl.beks.rental.entities.City;
import pl.beks.rental.entities.Users;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsersFormModel {

    private Long id;

    @NotEmpty
    @Size(min=4, max=40)
    private String login;

    @NotEmpty
    @Size(min=4, max=8)
    private String password;

    public UsersFormModel(Users usersEntity) {
        this.id = usersEntity.getId();
        this.login = usersEntity.getLogin();
        this.password = usersEntity.getPassword();
    }
}
