package pl.beks.rental.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.beks.rental.entities.Scooter;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScooterFormModel {

    private Long id;

    private Long cityId;

    private boolean rented;

    private LocalDateTime dateTimeRented;

    public ScooterFormModel(Scooter scooterEntity) {
        this.id = scooterEntity.getId();
        this.rented = scooterEntity.isRented();
        this.dateTimeRented = scooterEntity.getDateTimeRented();

    }
}