package pl.beks.rental.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.NotEmpty;
import pl.beks.rental.entities.City;


import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityFormModel {

    private Long id;

    @NotEmpty
    @Size(min=2, max=15)
    private String name;


    public CityFormModel(City cityEntity) {
        this.id = cityEntity.getId();
        this.name = cityEntity.getName();

    }
}