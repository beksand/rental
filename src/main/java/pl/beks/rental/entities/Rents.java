package pl.beks.rental.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Rents {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Scooter scooter;

    @ManyToOne
    private Users users;

    private LocalDateTime startRent;

    private LocalDateTime finishRent;

    private Double profit;


}
