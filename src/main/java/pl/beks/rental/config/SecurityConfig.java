package pl.beks.rental.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("1234").authorities("admin").and()
                .withUser("user").password("1234").authorities("user");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                    .antMatchers("/user").permitAll()
                    .antMatchers("/user/city/{cityId}/toRent").authenticated()
                    .antMatchers("/admin").hasAuthority("admin")
                .and()
                    .formLogin()
                    .loginProcessingUrl("/login")
                    .loginPage("/loginForm")
                    .defaultSuccessUrl("/user")
                    .failureUrl("/loginFailure")
                    .usernameParameter("username")
                    .passwordParameter("password")
                .and()
                    .logout().logoutUrl("/logout").logoutSuccessUrl("/user").and()
                    .exceptionHandling().accessDeniedPage("/accessDenied");
    }
}