package pl.beks.rental.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.beks.rental.entities.Users;

@Repository
public interface IUsersDAO extends CrudRepository<Users, Long>{
}
