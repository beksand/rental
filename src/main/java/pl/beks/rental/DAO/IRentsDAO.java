package pl.beks.rental.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.beks.rental.entities.Rents;

@Repository
public interface IRentsDAO extends CrudRepository<Rents, Long> {
}
