package pl.beks.rental.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.beks.rental.entities.City;

@Repository
public interface ICityDAO extends CrudRepository<City, Long>{
}
