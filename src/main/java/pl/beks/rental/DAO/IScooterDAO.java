package pl.beks.rental.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.beks.rental.entities.Scooter;

import java.util.List;

@Repository
public interface IScooterDAO extends CrudRepository<Scooter, Long>{

    List<Scooter> findAllByCity_Id(Long cityId);
}
